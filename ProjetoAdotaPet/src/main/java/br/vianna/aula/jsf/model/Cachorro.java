/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author PedroVitorDev
 */
@Entity
@PrimaryKeyJoinColumn(name = "idcachorro")
public class Cachorro extends Pet {

    
  @NotNull
  @Enumerated(EnumType.STRING)
  private PorteCachorro porte;  
    
    
  @NotNull
  @Enumerated(EnumType.STRING)
  private RacaCachorro raca;

    public Cachorro() {
    }
  
    public Cachorro(PorteCachorro porte, RacaCachorro raca) {
        this.porte = porte;
        this.raca = raca;
    }

    public Cachorro(PorteCachorro porte, RacaCachorro raca, int idpet, String nome, String sexo, Date dataNasc, boolean ehDomesticado, boolean ehVacinado, boolean ehCadastrado, Adotante adotante) {
        super(idpet, nome, sexo, dataNasc, ehDomesticado, ehVacinado, ehCadastrado, adotante);
        this.porte = porte;
        this.raca = raca;
    }

    public PorteCachorro getPorte() {
        return porte;
    }

    public void setPorte(PorteCachorro porte) {
        this.porte = porte;
    }

    public RacaCachorro getRaca() {
        return raca;
    }

    public void setRaca(RacaCachorro raca) {
        this.raca = raca;
    }

       

}
