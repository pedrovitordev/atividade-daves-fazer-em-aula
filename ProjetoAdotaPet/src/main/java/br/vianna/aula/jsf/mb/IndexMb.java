/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.mb;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author PedroVitorDev
 */
@Component
@RequestScoped

public class IndexMb {

    private String nomeProjeto;
    
    @Autowired
    private LoginMB loginMb;
    

    public IndexMb() {

        nomeProjeto = "Adota Pet :: HOME - New";

    }
    
    @PostConstruct
    public void init(){
    
        if(!loginMb.isIsLogado()){
            
            try {
                
                loginMb.setMsgErro("Acesso não autorizado!!");               
                FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
            } catch (IOException ex) {           
            }
            
            
        }  
    } //injeção de dependencias
    
    
    
    public String getNomeProjeto() {
        return nomeProjeto;
    }

    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    public void setLoginMb(LoginMB loginMb) {
        this.loginMb = loginMb;
    }

    
    
}
