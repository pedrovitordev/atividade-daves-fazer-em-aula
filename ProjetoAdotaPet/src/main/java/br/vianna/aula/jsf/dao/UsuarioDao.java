/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.dao;

import br.vianna.aula.jsf.model.Usuario;
import br.vianna.aula.jsf.model.dto.UsuarioLogadoDTO;
import br.vianna.aula.jsf.util.Utils;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import static sun.security.jgss.GSSUtil.login;

/**
 *
 * @author PedroVitorDev
 */
@Component
public class UsuarioDao {

    @Autowired
    private EntityManager con;

    @Transactional
    public Usuario save(Usuario u) {

        con.persist(u);

        return u;

    }

    public UsuarioLogadoDTO existeUsuario(String login, String senha) {
        
        
    try {
        Query q = con.createQuery("select new br.vianna.aula.jsf.model.dto.UsuarioLogadoDTO(u.id,u.nome,u.email,u.tipo) "
                + "from Usuario u where u.login = :log"
                + " and u.senha = :sen"); //essa consulta vai retornar um usuario logado DTO

        q.setParameter("log", login);
        q.setParameter("sen",Utils.md5(senha) );
        
        return (UsuarioLogadoDTO) q.getSingleResult();
        
        } catch (Exception e) {
            return null;
        }

    }

}
