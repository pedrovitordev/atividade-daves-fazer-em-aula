/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.dao;

import br.vianna.aula.jsf.model.Cachorro;
import br.vianna.aula.jsf.model.Gato;
import br.vianna.aula.jsf.model.Pet;
import br.vianna.aula.jsf.model.Usuario;
import br.vianna.aula.jsf.model.dto.ListPetDTO;
import br.vianna.aula.jsf.model.dto.UsuarioLogadoDTO;
import br.vianna.aula.jsf.util.Utils;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import static sun.security.jgss.GSSUtil.login;

/**
 *
 * @author PedroVitorDev
 */
@Component
public class GatoDao {

    
    private String src = "br.vianna.aula.jsf.model.dto.";
    
    @Autowired
    private EntityManager con;

    @Transactional
    public Gato save(Gato c) {

        if(c.getIdpet() > 0){
            con.merge(c);        
        }else{            
            con.persist(c);
        }

        return c;

    }

    public ArrayList<ListPetDTO> getAllPets() {

        Query q = con.createQuery("select new " + src + "ListPetDTO(p.idpet, p.nome,"
                + "p.sexo, 'Gato', a.nome, p.ehDomesticado) "
                + "from Gato p"
                + " left join p.adotante a ");

        return (ArrayList<ListPetDTO>) q.getResultList();

    }

    public Gato get(int id) {
         return con.find(Gato.class, id);
    }

    
    @Transactional
    public Gato delete(int id) {

        Gato c = get(id);

        con.remove(c);
        return c;

    }

}
