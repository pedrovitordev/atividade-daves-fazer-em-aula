/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.mb;

import br.vianna.aula.jsf.dao.CachorroDao;
import br.vianna.aula.jsf.dao.GatoDao;
import br.vianna.aula.jsf.dao.PetDao;
import br.vianna.aula.jsf.model.Cachorro;
import br.vianna.aula.jsf.model.Gato;
import br.vianna.aula.jsf.model.PelagemGato;
import br.vianna.aula.jsf.model.Pet;
import br.vianna.aula.jsf.model.PorteCachorro;
import br.vianna.aula.jsf.model.RacaCachorro;
import br.vianna.aula.jsf.model.RacaGato;
import br.vianna.aula.jsf.model.dto.ListPetDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author PedroVitorDev
 */
@Component(value = "cadPetMB")
@ViewScoped
public class CadastroPetMB implements Serializable {

    private EStatusCrud status;

    private ArrayList<ListPetDTO> lista;

    private Pet animal;

    private String tipoEscolhido;

    private PorteCachorro[] porteCachorro;
    private RacaCachorro[] racaCachorro;
    private RacaGato[] racaGato;
    private PelagemGato[] pelagemGato;

   

    @Autowired
    private CachorroDao cacDao;

    @Autowired
    private GatoDao gatoDao;

    public CadastroPetMB() {

        status = EStatusCrud.VIEW;


        porteCachorro = PorteCachorro.values();
        racaCachorro = RacaCachorro.values();
        racaGato = RacaGato.values();
        pelagemGato = PelagemGato.values();

        inicializaAnimal();
        ((Cachorro) animal).setPorte(PorteCachorro.Pequeno);
        

//        lista = new ArrayList<>();
//        lista.add(new ListPetDTO("Fofinho", "Feminino", "Gato", "Zezin", true));
//        lista.add(new ListPetDTO("Bravão", "Masculino", "Cachorro", null, true));
//        lista.add(new ListPetDTO("Lili", "Feminino", "Cachorro", null, true));
//        lista.add(new ListPetDTO("Bolinho", "Masculino", "Gato", null, true));
//        lista.add(new ListPetDTO("Mostarda", "Masculino", "Cachorro", "Gustin", true));
//        lista.add(new ListPetDTO("Azulão", "Masculino", "Cachorro", null, true));
    }

    public void changeTipo() {

        System.out.println("chamou" + tipoEscolhido);
        if (tipoEscolhido.equalsIgnoreCase("c")) {
            animal = new Cachorro();
        } else {
            animal = new Gato();
        }

    }

    private void inicializaAnimal() {
        animal = new Cachorro();
//        animal.setNome("abc");
        animal.setSexo("Masculino");
        animal.setEhVacinado(true);
        animal.setDataNasc(new Date());
        tipoEscolhido = "c";
    }

    @PostConstruct
    public void init() {

        lista = getAllPets();

    }

    public void validaDataNascimento(FacesContext ct, UIComponent comp, Object value) {

        Date data = (Date) value;

        if (data.after(new Date())) {

            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Data incorreta", "A data não pode ser futura"));
        }

    }
    
    public String preparedEdit(int id, String tipo){
    
        status = EStatusCrud.EDIT;
        
        
         if (tipo.equalsIgnoreCase("Cachorro")) {
            animal = cacDao.get(id);
            tipoEscolhido = "c";
        } else {
            animal = gatoDao.get(id);
            tipoEscolhido = "g";
        }
               
        return "";
    
    }
        
     public String delete(int id, String tipo){
 
         FacesContext ct = FacesContext.getCurrentInstance();
         
         
         Pet aux = null;
         
         if(tipo.equalsIgnoreCase("Cachorro")){
            aux= cacDao.delete(id);
         }else{
            aux= gatoDao.delete(id);
         }
         
        lista = getAllPets(); 
         
        ct.addMessage("", new FacesMessage(aux.getNome()+" foi apagado com sucesso!!"));

        return "";
         
     } 
             
     
    
    
    public String salvar() {

        FacesContext ct = FacesContext.getCurrentInstance();

        if (animal instanceof Cachorro && ((Cachorro) animal).getPorte() == PorteCachorro.Grande) {
            ct.addMessage("", new FacesMessage("não aceitamos cachorro Grande"));
            return "";
        }

        String tp="";
        if (animal instanceof Cachorro) {

            cacDao.save((Cachorro) animal);
            tp="Cachorro";

        } else {
            gatoDao.save((Gato) animal);
            tp="Gato";
        }

        inicializaAnimal();

        status = EStatusCrud.VIEW;

        lista = getAllPets();
        
        ct.addMessage("", new FacesMessage(tp+" salvo com sucesso!!"));

        return "";

    }

    public String trocar() {

        status = EStatusCrud.INSERT;
        
        inicializaAnimal();
        
        return "";
    }

    public String voltar() {

        status = EStatusCrud.VIEW;

        return "";
    }

     private ArrayList<ListPetDTO> getAllPets() {
        
       ArrayList<ListPetDTO> lista = new ArrayList<>();
       
       lista.addAll(cacDao.getAllPets());
       lista.addAll(gatoDao.getAllPets());
       
       return lista;
        
    }
    
    
    
    
    public boolean isView() {

        return status == EStatusCrud.VIEW;
    }

    public EStatusCrud getStatus() {
        return status;
    }

    public void setStatus(EStatusCrud status) {
        this.status = status;
    }

    public ArrayList<ListPetDTO> getLista() {
        return lista;
    }

    public void setLista(ArrayList<ListPetDTO> lista) {
        this.lista = lista;
    }

    public Pet getAnimal() {
        return animal;
    }

    public void setAnimal(Pet animal) {
        this.animal = animal;
    }

    public String getTipoEscolhido() {
        return tipoEscolhido;
    }

    public void setTipoEscolhido(String tipoEscolhido) {
        this.tipoEscolhido = tipoEscolhido;
    }


    public PorteCachorro[] getPorteCachorro() {
        return porteCachorro;
    }

    public void setPorteCachorro(PorteCachorro[] porteCachorro) {
        this.porteCachorro = porteCachorro;
    }

    public RacaCachorro[] getRacaCachorro() {
        return racaCachorro;
    }

    public void setRacaCachorro(RacaCachorro[] racaCachorro) {
        this.racaCachorro = racaCachorro;
    }

    public RacaGato[] getRacaGato() {
        return racaGato;
    }

    public void setRacaGato(RacaGato[] racaGato) {
        this.racaGato = racaGato;
    }

    public PelagemGato[] getPelagemGato() {
        return pelagemGato;
    }

    public void setPelagemGato(PelagemGato[] pelagemGato) {
        this.pelagemGato = pelagemGato;
    }

   

}
