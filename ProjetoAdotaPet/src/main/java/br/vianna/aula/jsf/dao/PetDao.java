/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.dao;

import br.vianna.aula.jsf.model.Usuario;
import br.vianna.aula.jsf.model.dto.ListPetDTO;
import br.vianna.aula.jsf.model.dto.UsuarioLogadoDTO;
import br.vianna.aula.jsf.util.Utils;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import static sun.security.jgss.GSSUtil.login;

/**
 *
 * @author PedroVitorDev
 */
@Component
public class PetDao {

    private String src = "br.vianna.aula.jsf.model.dto.";

    @Autowired
    private EntityManager con;

    public ArrayList<ListPetDTO> getAllPets() {

        Query q = con.createQuery("select new " + src + "ListPetDTO(p.idpet, p.nome,"
                + "p.sexo, '-', a.nome, p.ehDomesticado) "
                + "from Pet p"
                + " left join p.adotante a ");

        return (ArrayList<ListPetDTO>) q.getResultList();

    }

}
