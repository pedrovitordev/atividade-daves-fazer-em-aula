/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author PedroVitorDev
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name= "pet")
public abstract class Pet{

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int idpet;

    @NotNull
    private String nome;

    @NotNull
    private String sexo;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date dataNasc;

    @NotNull
    private boolean ehDomesticado;
    
    @NotNull
    private boolean ehVacinado;
    
    @NotNull
    private boolean ehCadastrado;

    @JoinColumn(name = "adotante", referencedColumnName = "idadotante")
    @ManyToOne(optional = true)
    private Adotante adotante;
    
    
    public Pet() {
    }

    public Pet(int idpet, String nome, String sexo, Date dataNasc, boolean ehDomesticado, boolean ehVacinado, boolean ehCadastrado, Adotante adotante) {
        this.idpet = idpet;
        this.nome = nome;
        this.sexo = sexo;
        this.dataNasc = dataNasc;
        this.ehDomesticado = ehDomesticado;
        this.ehVacinado = ehVacinado;
        this.ehCadastrado = ehCadastrado;
        this.adotante = adotante;
    }

    public int getIdpet() {
        return idpet;
    }

    public void setIdpet(int idpet) {
        this.idpet = idpet;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(Date dataNasc) {
        this.dataNasc = dataNasc;
    }

    public boolean isEhDomesticado() {
        return ehDomesticado;
    }

    public void setEhDomesticado(boolean ehDomesticado) {
        this.ehDomesticado = ehDomesticado;
    }

    public boolean isEhVacinado() {
        return ehVacinado;
    }

    public void setEhVacinado(boolean ehVacinado) {
        this.ehVacinado = ehVacinado;
    }

    public boolean isEhCadastrado() {
        return ehCadastrado;
    }

    public void setEhCadastrado(boolean ehCadastrado) {
        this.ehCadastrado = ehCadastrado;
    }

    public Adotante getAdotante() {
        return adotante;
    }

    public void setAdotante(Adotante adotante) {
        this.adotante = adotante;
    }

   
}
