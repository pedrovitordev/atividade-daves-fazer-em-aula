/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author PedroVitorDev
 */
@Entity
@Table(name = "adotante")
public class Adotante {

    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int idadotante;
    
    @NotNull
    private String nome;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date dataNasc;
    
    @NotNull
    private int salario;
    
    @NotNull
    private ArrayList<Pet> p;

    public Adotante() {
    }

    public Adotante(int idadotante, String nome, Date dataNasc, int salario, ArrayList<Pet> p) {
        this.idadotante = idadotante;
        this.nome = nome;
        this.dataNasc = dataNasc;
        this.salario = salario;
        this.p = p;
    }

    public int getIdadotante() {
        return idadotante;
    }

    public void setIdadotante(int idadotante) {
        this.idadotante = idadotante;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(Date dataNasc) {
        this.dataNasc = dataNasc;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public ArrayList<Pet> getP() {
        return p;
    }

    public void setP(ArrayList<Pet> p) {
        this.p = p;
    }

    

}
