/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author PedroVitorDev
 */
@Entity
@PrimaryKeyJoinColumn(name = "idgato")
public class Gato extends Pet {

 @NotNull
 @Enumerated(EnumType.STRING)
 private RacaGato raca;
 
 @NotNull
 @Enumerated(EnumType.STRING)
 private PelagemGato pelagem;

    public Gato() {
    }

    public Gato(RacaGato raca, PelagemGato pelagem) {
        this.raca = raca;
        this.pelagem = pelagem;
    }

    public Gato(RacaGato raca, PelagemGato pelagem, int idpet, String nome, String sexo, Date dataNasc, boolean ehDomesticado, boolean ehVacinado, boolean ehCadastrado, Adotante adotante) {
        super(idpet, nome, sexo, dataNasc, ehDomesticado, ehVacinado, ehCadastrado, adotante);
        this.raca = raca;
        this.pelagem = pelagem;
    }

    public RacaGato getRaca() {
        return raca;
    }

    public void setRaca(RacaGato raca) {
        this.raca = raca;
    }

    public PelagemGato getPelagem() {
        return pelagem;
    }

    public void setPelagem(PelagemGato pelagem) {
        this.pelagem = pelagem;
    }

   

}
