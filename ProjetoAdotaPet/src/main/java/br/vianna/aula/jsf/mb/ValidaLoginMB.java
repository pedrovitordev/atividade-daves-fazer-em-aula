/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.mb;

import br.vianna.aula.jsf.dao.UsuarioDao;
import br.vianna.aula.jsf.model.ETipoUsuario;
import br.vianna.aula.jsf.model.Usuario;
import br.vianna.aula.jsf.util.Utils;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author PedroVitorDev
 */
@Component
@RequestScoped

public class ValidaLoginMB {

    @Autowired
    UsuarioDao userD;
       
    @Autowired
    private LoginMB loginMb;

    public String verificaPassword() {

        loginMb.setUser(userD.existeUsuario(loginMb.getNome(), loginMb.getSenha()));

        if (loginMb.getUser() != null) {
            loginMb.setIsLogado(true);

            return "index";

        } else {
            loginMb.setMsgErro("login ou senha incorreta!!");
            loginMb.setNome("");
            loginMb.setSenha("");

            return "";
        }

    }
}
