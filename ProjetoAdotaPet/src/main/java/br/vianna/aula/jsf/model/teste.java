/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author PedroVitorDev
 */




@Entity
@Table(name="Teste")
public class teste {
    
    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int id;
    private String teste;
    private double teste2;

    public teste() {
    }

    public teste(double teste2) {
        this.teste2 = teste2;
    }

    public String getTeste() {
        return teste;
    }

    public void setTeste(String teste) {
        this.teste = teste;
    }

    public double getTeste2() {
        return teste2;
    }

    public void setTeste2(double teste2) {
        this.teste2 = teste2;
    }
    
    
    
    
    
    
    
    
    
}
