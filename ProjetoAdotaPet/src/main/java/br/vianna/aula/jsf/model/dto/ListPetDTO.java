/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.model.dto;

/**
 *
 * @author PedroVitorDev
 */
public class ListPetDTO {

    private int id;

    private String nome, sexo, tipo, nomeAdotante;

    private boolean ehDomesticado;

    public ListPetDTO() {
    }

    public ListPetDTO(int id, String nome, String sexo, String tipo, String nomeAdotante, boolean ehDomesticado) {
        this.id = id;
        this.nome = nome;
        this.sexo = sexo;
        this.tipo = tipo;
        this.nomeAdotante = nomeAdotante;
        this.ehDomesticado = ehDomesticado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isEhDomesticado() {
        return ehDomesticado;
    }

    public void setEhDomesticado(boolean ehDomesticado) {
        this.ehDomesticado = ehDomesticado;
    }

    public String getNomeAdotante() {
        return nomeAdotante;
    }

    public void setNomeAdotante(String nomeAdotante) {
        this.nomeAdotante = nomeAdotante;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
