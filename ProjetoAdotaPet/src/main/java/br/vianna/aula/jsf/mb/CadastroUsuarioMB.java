/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.mb;

import br.vianna.aula.jsf.dao.UsuarioDao;
import br.vianna.aula.jsf.model.ETipoUsuario;
import br.vianna.aula.jsf.model.Usuario;
import br.vianna.aula.jsf.util.Utils;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author PedroVitorDev
 */
@Component(value = "cadMB")
@RequestScoped

public class CadastroUsuarioMB {

    private Usuario user;

    @Autowired
    UsuarioDao userD;
    
    @Autowired    //injeçaõ de dependencia
    LoginMB login;
    

    public CadastroUsuarioMB() {

        user = new Usuario();

    }

    public String saveUser() {

        user.setTipo(ETipoUsuario.NORMAL);
        try {
            user.setSenha(Utils.md5(user.getSenha())); //passando a senha do cadastro pra md5
        } catch (NoSuchAlgorithmException ex) {

        }

        userD.save(user);

        user = new Usuario(); //retornar pagina em branco pra poder inserir um novo usuario

        login.setMsgAviso("Usuário criado com sucesso!!");
        
        return "login?facess-redirect=true"; //redirect

    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

}
