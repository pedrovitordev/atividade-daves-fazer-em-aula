package br.vianna.aula.jsf;

import br.vianna.aula.jsf.dao.CachorroDao;
import br.vianna.aula.jsf.dao.UsuarioDao;
import br.vianna.aula.jsf.model.Cachorro;
import br.vianna.aula.jsf.model.ETipoUsuario;
import br.vianna.aula.jsf.model.PorteCachorro;
import br.vianna.aula.jsf.model.RacaCachorro;
import br.vianna.aula.jsf.model.Usuario;
import br.vianna.aula.jsf.util.Utils;
import ch.qos.logback.classic.pattern.Util;
import java.time.LocalDate;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectJsfSpringApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ProjectJsfSpringApplication.class, args);
    }

    @Autowired
    UsuarioDao userD;   //usando a Dao pra salvar, injeção de dependencia

    
    @Autowired
     CachorroDao cachorroD;
    
    
    
    @Override
    public void run(String... args) throws Exception {

        System.out.println("Servidor on!!");

        System.out.println("a ::" + Utils.md5("a"));

        Usuario u = new Usuario(0, "Zezin", "ze@ze", "ze", Utils.md5("123"), ETipoUsuario.ADMIN);
        Usuario u1 = new Usuario(0, "Pedrin", "pe@pe", "ped", Utils.md5("123"), ETipoUsuario.NORMAL);

        userD.save(u); //Salvando
        userD.save(u1);
        
       Cachorro c = new Cachorro(PorteCachorro.Pequeno, RacaCachorro.Pincher, 0, "Fofin", 
               "Masculino", new Date(), true, true, true, null);
        
        cachorroD.save(c);
        

    }

}
 