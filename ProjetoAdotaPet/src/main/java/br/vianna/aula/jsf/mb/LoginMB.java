/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.vianna.aula.jsf.mb;

import br.vianna.aula.jsf.dao.UsuarioDao;
import br.vianna.aula.jsf.model.ETipoUsuario;
import br.vianna.aula.jsf.model.Usuario;
import br.vianna.aula.jsf.model.dto.UsuarioLogadoDTO;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import org.graalvm.compiler.lir.CompositeValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author PedroVitorDev
 */
@Component
@SessionScoped
public class LoginMB implements Serializable {

    private String nome, senha, msgErro, msgAviso;
    private boolean isLogado;
    private UsuarioLogadoDTO user; //Variavel auxiliar pra poder usar depois
    //guardando usuario logado na sessão

    @Autowired
    UsuarioDao userD;

    public LoginMB() {
        msgErro = msgAviso = "";
        isLogado = false;
        user = null;
    }

    public boolean isAdmin() {

        if (user == null) {
            return false;

        }
        return user.getTipo() == ETipoUsuario.ADMIN;

    }

    public boolean isNormal() {

        if (user == null) {
            return false;

        }
        return user.getTipo() == ETipoUsuario.NORMAL;

    }

    public String callCadastro() {

        return "cadastro?faces-redirect=true";

    }

    public String getMsgErro() {
        return msgErro;
    }

    public void setMsgErro(String msgErro) {
        this.msgErro = msgErro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isIsLogado() {
        return isLogado;
    }

    public void setIsLogado(boolean isLogado) {
        this.isLogado = isLogado;
    }

    public String getMsgAviso() {
        return msgAviso;
    }

    public void setMsgAviso(String msgAviso) {
        this.msgAviso = msgAviso;
    }

    public UsuarioLogadoDTO getUser() {
        return user;
    }

    public void setUser(UsuarioLogadoDTO user) {
        this.user = user;
    }

}
